﻿Shader "Custom/DistanceBasedColorFixed"
{
	Properties
	{
		_FixedColorNear("Near Color", Color) = (0,0,0,0)
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		// make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float3 worldPos : TEXCOORD1;
		float4 vertex : SV_POSITION;
	};

	uniform float4 _PlayerPos;
	uniform float _FarPlane;
	fixed4 _ColorFar;
	fixed4 _FixedColorNear;

	v2f vert(appdata v)
	{
		v2f o;

		o.worldPos = mul(unity_ObjectToWorld, v.vertex);
		o.vertex = UnityObjectToClipPos(v.vertex);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		fixed4 col;
		float dist = distance(_PlayerPos, i.worldPos);
		col = lerp(_FixedColorNear, _ColorFar, clamp((dist / _FarPlane),0,1));
		return col;
	}
		ENDCG
	}
	}
}
