﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemiesComponent : MonoBehaviour {

    AudioSource source;
    public AudioClip onHurt;
    public GameObject onDie;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnHurt()
    {
        if(source)
        {
            AudioSource tempSource = PlayClipAt(onHurt, transform.position);
            tempSource.pitch = source.pitch + Random.Range(-0.3f, 03f);
            tempSource.PlayOneShot(onHurt);
            tempSource.spatialBlend = 1;
            tempSource.maxDistance = 80;
            tempSource.volume = 0.08f;
        }

        gameObject.transform.DOScale(0, 0.2f).OnComplete(() => {
            Destroy(gameObject);
        });
    }

    AudioSource PlayClipAt( AudioClip clip, Vector3 pos)
    {
        GameObject obj = Instantiate(onDie, pos, Quaternion.identity);
        var tempGO = new GameObject();
        obj.transform.parent = tempGO.transform;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.LookAt(GetComponent<Boids>().target.transform,Vector3.up);
        tempGO.transform.position = pos; // set its position
        var aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        // set other aSource properties here, if desired
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }
}
