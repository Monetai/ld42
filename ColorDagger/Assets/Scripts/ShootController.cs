﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[ExecuteInEditMode]
public class ShootController : MonoBehaviour {

    public Transform origin;
    public GameObject projectile;
    public float spawnRadius = 0.5f;
    public float fireRate = 0.2f;
    public float decayRate = 0.05f;

    private float nextFire;
    private AudioSource source;
    float basePitch;
    private float initialTime;
    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
        basePitch = source.pitch;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetMouseButton(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Vector3 spawnPos = origin.position + (Vector3)Random.insideUnitCircle * spawnRadius;

            Instantiate(projectile, spawnPos, Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), transform.forward) * origin.rotation);

            if (source != null)
            {
                source.pitch = basePitch + Random.Range(-0.3f, 0.3f);
                source.PlayOneShot(source.clip, 0.5f);
            }

            GlobalShaderSetter.farPlane -= 0.3f;
            GlobalShaderSetter.farPlane = Mathf.Clamp(GlobalShaderSetter.farPlane, 0.5f, 100);
            GetComponent<ScreenShake>().shakeDuration += 0.1f;

            Camera.main.GetComponent<ScreenShake>().shakeDuration += 0.1f;

            GlobalShaderSetter.instance.BlinkColor();
        }

        //if (Input.GetMouseButtonDown(0))
        //{
        //    initialTime = Time.time;
        //}

        //if (Input.GetMouseButtonUp(0))
        //{
        //    Vector3 spawnPos = origin.position + (Vector3)Random.insideUnitCircle * spawnRadius;

        //    GameObject obj = Instantiate(projectile, spawnPos, Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), transform.forward) * origin.rotation);

        //    if (source != null)
        //        source.PlayOneShot(source.clip, 0.5f);

        //    GlobalShaderSetter.farPlane -= 0.5f;
        //    GlobalShaderSetter.farPlane = Mathf.Clamp(GlobalShaderSetter.farPlane, 0.5f, 100);
        //    GetComponent<ScreenShake>().shakeDuration += 0.1f;

        //    Camera.main.GetComponent<ScreenShake>().shakeDuration += 0.1f;
        //}

    }
}
