﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GroundScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale = new Vector3(GlobalShaderSetter.farPlane * 2, transform.localScale.y, GlobalShaderSetter.farPlane * 2);
	}
}
