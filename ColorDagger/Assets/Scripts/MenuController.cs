﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuController : MonoBehaviour {

	// Use this for initialization
	void Awake () {

        FadeController.instance.ToWhite(null);

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
     }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        FadeController.instance.ToBlack(() => {
            SceneManager.LoadScene("InGameScene");
        });
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ChangeColor()
    {
        GlobalShaderSetter.instance.GetNewColors();
    }

    public void ResetColors()
    {
        GlobalShaderSetter.instance.ResetColors();
    }
}
