﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameOverController : MonoBehaviour {

    public TextMeshProUGUI text;
    public Camera cam;
    public List<TextMeshProUGUI> textes;
	// Use this for initialization

	void Start ()
    {
        FadeController.instance.ToWhite(null);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        float score = PlayerPrefs.GetFloat("score", 0) ;
        float bestScore = PlayerPrefs.GetFloat("bestScore", 0);
        string scoretext = string.Format("{0}:{1:00}", (int)score / 60, (int)score % 60);
        string bestScoretext = string.Format("{0}:{1:00}", (int)bestScore / 60, (int)bestScore % 60);

        text.text = "You survived " + scoretext + " min, You best was " + bestScoretext + " min.";

        foreach(TextMeshProUGUI tex in textes)
        {
            Color col = new Color();
            col.r = PlayerPrefs.GetFloat("nearR", 0);
            col.g = PlayerPrefs.GetFloat("nearG", 0);
            col.b = PlayerPrefs.GetFloat("nearB", 0);
            col.a = 1;
            tex.faceColor = col;
        }

        Color color = new Color();

        color.r = PlayerPrefs.GetFloat("farR", 0);
        color.g = PlayerPrefs.GetFloat("farG", 0);
        color.b = PlayerPrefs.GetFloat("farB", 0);

        cam.backgroundColor = color;
    }
	
	public void GoToMenu()
    {
        FadeController.instance.ToBlack(() => {
            SceneManager.LoadScene("Menu");
        });
    }

    public void Retry()
    {
        FadeController.instance.ToBlack(() => {
            SceneManager.LoadScene("InGameScene");
        });
    }
}
