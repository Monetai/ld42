﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Characters;
using TMPro;
public class InGameController : MonoBehaviour {

    public static InGameController instance;
    bool haveDie = false;
    public AudioClip onDie;
    private float time;
    public TextMeshProUGUI text;

    private void Awake()
    {
        instance = this;
        time = Time.time;
    }

    // Use this for initialization
    void Start () {
        FadeController.instance.ToWhite(null);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    bool godMode = false;
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -2)
            Die();
        GetComponent<FirstPersonController>();

        string scoretext = string.Format("{00}:{1:00}", (int)(Time.time - time) / 60, (int)(Time.time - time) % 60);

        text.text =  scoretext ;
    }

    void Die()
    {
        if (godMode)
            return;

        if(haveDie)
        {
            return;
        }
        else
        {
            haveDie = true;
        }

        AudioSource.PlayClipAtPoint(onDie, transform.position);

        PlayerPrefs.SetFloat("score", Time.time - time);

        float best = PlayerPrefs.GetFloat("bestScore", 0);

        if(Time.time - time > best)
        {
            PlayerPrefs.SetFloat("bestScore", Time.time - time);
        }

        FadeController.instance.ToBlackImmediate(() => 
            {
                SceneManager.LoadScene("GameOver");
            },2f
        );
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Enemies"))
        {
            Die();
        }
    }
}
