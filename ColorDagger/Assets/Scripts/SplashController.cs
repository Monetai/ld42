﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        FadeController.instance.ToWhite(null);
        Invoke("Next", 3f);

    }
	
	// Update is called once per frame
	void Next () {
        FadeController.instance.ToBlack(() =>
            {
                SceneManager.LoadScene("Menu");
            }
        );
    }
}
