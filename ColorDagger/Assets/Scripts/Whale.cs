﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whale : MonoBehaviour
{
    public float Mass = 15;
    public float MaxSpeed = 3;
    public float MaxForce = 15;

    public float detectionLength = 6;
    public float avoidanceStrenght = 50;

    public int lenght = 20;
    public float minSize = 0.5f;

    public GameObject followerPrefab;
    private Vector3 velocity;
    private Vector3 wanderForce;
    private Vector3 target;

    private bool mustGo = false;
    private List<WhaleFollower> followers;
    private float baseRadius;
    private void Awake()
    {
        baseRadius = GlobalShaderSetter.farPlane;
        target = GetFreePoint();

        followers = new List<WhaleFollower>();
        float maxScale = transform.localScale.x;

        float scaleStep = maxScale / lenght;
        float curScale = maxScale;
        for(int i = 0; i < lenght; i++)
        {
            curScale -= scaleStep;
            GameObject obj = Instantiate(followerPrefab, transform.position, transform.rotation);
            obj.transform.localScale = new Vector3(curScale, curScale, curScale);
            WhaleFollower foll = obj.GetComponent<WhaleFollower>();
            if(i == 0)
            {
                foll.toFollow = this.gameObject;
            }
            else
            {
                foll.toFollow = followers[i-1].gameObject;
            }
            followers.Add(foll);
        }
    }

    public Vector3 GetFreePoint()
    {
        Vector3 tempTarget;
        tempTarget = Random.insideUnitSphere * GlobalShaderSetter.farPlane;
        tempTarget.y = Mathf.Clamp(target.y, 2, 30);
        if(Physics.OverlapSphere(tempTarget, 0.5f, LayerMask.GetMask("Default")).Length > 0)
        {
            return GetFreePoint();
        }
        else
        {
            return tempTarget;
        }
    }

    private void FixedUpdate()
    {
        if (Vector3.Distance(transform.position,target) < transform.localScale.x+0.5f)
        {
            target = GetFreePoint();
        }

        var desiredVelocity = GetWanderForce();
        desiredVelocity = desiredVelocity.normalized * MaxSpeed;

        var steeringForce = desiredVelocity - velocity;
        steeringForce = Vector3.ClampMagnitude(steeringForce, MaxForce);
        steeringForce /= Mass;

        velocity = Vector3.ClampMagnitude(velocity + steeringForce, MaxSpeed);
        transform.position += velocity * Time.deltaTime;
        transform.forward = velocity.normalized;

        Debug.DrawRay(transform.position, velocity.normalized * 2, Color.green);
        Debug.DrawRay(transform.position, desiredVelocity.normalized * 2, Color.magenta);

    }

    private Vector3 GetWanderForce()
    {
        Collider[] boids;
        Vector3 separation = Vector3.zero;

        boids = Physics.OverlapSphere(transform.position, 4);
        int count = 1;
        foreach (var boid in boids)
        {
            if (boid.GetComponent<Boids>() == null)
                continue;

            if (boid != GetComponent<Collider>() && (transform.position - boid.transform.position).magnitude < 1)
            {
                if ((transform.position - boid.transform.position).magnitude != 0)
                {
                    separation += (transform.position - boid.transform.position) / (transform.position - boid.transform.position).magnitude;
                    count++;
                }
            }
        }


        RaycastHit hit = new RaycastHit();
        int layer_mask = LayerMask.GetMask("Default");

        Vector3 avoidanceDir = Vector3.zero;


        if (Physics.SphereCast(transform.position, transform.localScale.x, velocity.normalized, out hit, detectionLength, layer_mask))
        {
            Vector3 temp = Vector3.Cross(hit.normal, velocity.normalized);
            avoidanceDir = Vector3.Cross(temp, hit.normal);

            avoidanceDir += avoidanceDir.normalized * ((( detectionLength - hit.distance) /  detectionLength) * avoidanceStrenght);
        }
        else if (Physics.SphereCast(transform.position, transform.localScale.x, transform.right, out hit, transform.localScale.x + 1, layer_mask))
        {
            Vector3 temp = Vector3.Cross(hit.normal, velocity.normalized);
            avoidanceDir = Vector3.Cross(temp, hit.normal);
            
            avoidanceDir += avoidanceDir.normalized * (((transform.localScale.x + 1 - hit.distance) / (transform.localScale.x + 1)) * avoidanceStrenght);
        }
        else if (Physics.SphereCast(transform.position, transform.localScale.x, -transform.right, out hit, transform.localScale.x + 1, layer_mask))
        {
            Vector3 temp = Vector3.Cross(hit.normal, velocity.normalized);
            avoidanceDir = Vector3.Cross(temp, hit.normal);

            avoidanceDir += avoidanceDir.normalized * (((transform.localScale.x + 1 - hit.distance) / (transform.localScale.x + 1)) * avoidanceStrenght);
        }


        var directionToCenter = ((target + Vector3.up * 2) - transform.position).normalized;


        if (hit.collider)
        {
            wanderForce += Vector3.Lerp(directionToCenter, avoidanceDir, 1);
        }
        else
        {
            wanderForce = velocity.normalized + directionToCenter;
            wanderForce += separation / count;
        }

        if (transform.position.y < 2)
            wanderForce += Vector3.up * 50;

        if (transform.position.y > 30 && mustGo == false)
            wanderForce += -Vector3.up * 20;


        return wanderForce;
    }

    private void OnDestroy()
    {
        SpawnerController.usedWhaleTiket--;
        GlobalShaderSetter.farPlane += 10f;
        GlobalShaderSetter.farPlane = Mathf.Clamp(GlobalShaderSetter.farPlane, 0.5f, 60);
    }
}