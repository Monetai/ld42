﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FadeController : MonoBehaviour
{

    public static FadeController instance;
    public float fadeSpeed = 0.5f;

    private RawImage img;

    private void Awake()
    {
        instance = this;
        img = GetComponent<RawImage>();
    }

    public void ToBlack(Action callback)
    {
        img.DOFade(1, fadeSpeed).OnComplete(() => {
            if (callback != null)
                callback.Invoke();
        });
    }

    public void ToBlackImmediate(Action callback, float delay)
    {
        img.color = new Color(img.color.r, img.color.g, img.color.b, 1);
        DOVirtual.DelayedCall(delay, () =>
        {
            if (callback != null)
                callback.Invoke();
        }, true);
    }

    public void ToWhite(Action callback)
    {
        img.DOFade(0, fadeSpeed).OnComplete(() => {
            if (callback != null)
                callback.Invoke();
        });
    }
}
