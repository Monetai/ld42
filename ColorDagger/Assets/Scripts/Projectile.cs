﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Projectile : MonoBehaviour
{
    public float speed;
    public float lifeTime = 5f;
    private float startTime;
    private bool haveTouched;
	// Use this for initialization
	void Start () {
        startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += transform.forward * Time.deltaTime * speed;

        if(Time.time > startTime + lifeTime)
            gameObject.transform.DOScale(0, 0.3f).OnComplete(() => {
                Destroy(gameObject);
            });
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(haveTouched)
        {
            return;
        }
        else
        {
            haveTouched = true;
        }

        EnemiesComponent nmie = collision.collider.GetComponent<EnemiesComponent>();
        if (nmie)
        {
            nmie.OnHurt();
        }

        gameObject.transform.DOScale(0, 0.8f).OnComplete( () => {
            Destroy(gameObject);
        });
    }
}
