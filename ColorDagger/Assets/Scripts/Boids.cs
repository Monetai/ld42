﻿using UnityEngine;

public class Boids : MonoBehaviour
{
    public float Mass = 15;
    public float MaxSpeed = 3;
    public float MaxForce = 15;

    public float detectionLength = 6;
    public float avoidanceStrenght = 50;


    private Vector3 velocity;
    private Vector3 wanderForce;
    public GameObject target;


    float prevCheck;
    float refreshrate = 0.4f;


    private void Start()
    {
        prevCheck = Time.time;
    }

    private void Update()
    {
        var desiredVelocity = GetWanderForce();
        desiredVelocity = desiredVelocity.normalized * MaxSpeed;

        var steeringForce = desiredVelocity - velocity;
        steeringForce = Vector3.ClampMagnitude(steeringForce, MaxForce);
        steeringForce /= Mass;

        velocity = Vector3.ClampMagnitude(velocity + steeringForce, MaxSpeed);
        transform.position += velocity * Time.deltaTime;
        transform.forward = velocity.normalized;

        Debug.DrawRay(transform.position, velocity.normalized * 2, Color.green);
        Debug.DrawRay(transform.position, desiredVelocity.normalized * 2, Color.magenta);

    }
    Vector3 separation;
    int count = 1;

    bool touchedSomething = false;
    private Vector3 GetWanderForce()
    {
        if (Time.time > prevCheck +refreshrate)
        {
            touchedSomething = false;
            separation = Vector3.zero;
            prevCheck = Time.time;
            Collider[] boids;

            boids = Physics.OverlapSphere(transform.position, 4);
            foreach (var boid in boids)
            {
                if (boid.GetComponent<Boids>() == null)
                {
                    touchedSomething = true;
                    continue;
                }

                if (boid != GetComponent<Collider>() && (transform.position - boid.transform.position).magnitude < 1)
                {
                    if((transform.position - boid.transform.position).magnitude != 0)
                    {
                        separation += (transform.position - boid.transform.position) / (transform.position - boid.transform.position).magnitude;
                        count++;
                    }
                }
            }
        }


        RaycastHit hit = new RaycastHit();
        int layer_mask = LayerMask.GetMask("Default");

        Vector3 avoidanceDir = Vector3.zero;


        if (Physics.Raycast(transform.position, velocity.normalized, out hit,detectionLength, layer_mask))
        {
            Vector3 temp = Vector3.Cross(hit.normal, velocity.normalized);
            avoidanceDir = Vector3.Cross(temp, hit.normal);

            avoidanceDir += avoidanceDir.normalized * (((detectionLength - hit.distance)/ detectionLength) * avoidanceStrenght);
        }
        else if(touchedSomething && Physics.Raycast(transform.position, transform.right, out hit, 0.5f, layer_mask))
        {
            Vector3 temp = Vector3.Cross(hit.normal, velocity.normalized);
            avoidanceDir = Vector3.Cross(temp, hit.normal);

            avoidanceDir += avoidanceDir.normalized * (((0.5f - hit.distance) / 0.5f) * avoidanceStrenght);
        }
        else if (touchedSomething && Physics.Raycast(transform.position,  -transform.right, out hit, 0.5f, layer_mask))
        {
            Vector3 temp = Vector3.Cross(hit.normal, velocity.normalized);
            avoidanceDir = Vector3.Cross(temp, hit.normal);

            avoidanceDir += avoidanceDir.normalized * (((0.5f - hit.distance) / 0.5f) * avoidanceStrenght);
        }


        var directionToCenter = ((target.transform.position + Vector3.up) - transform.position).normalized;
        

        if(hit.collider)
        {
            wanderForce += Vector3.Lerp(directionToCenter, avoidanceDir, 1);
        }
        else
        {
            wanderForce = velocity.normalized + directionToCenter;
            wanderForce += (separation / count)*100;
        }



        if (transform.position.y < 2)
            wanderForce += Vector3.up * 20;

        if (transform.position.y > 30)
            wanderForce += -Vector3.up * 20;


        return wanderForce;
    }

    private void OnDestroy()
    {
        SpawnerController.usedSeekerTiket--;
        GlobalShaderSetter.farPlane += 0.3f;
        GlobalShaderSetter.farPlane = Mathf.Clamp(GlobalShaderSetter.farPlane, 0.5f, 100);
    }
}