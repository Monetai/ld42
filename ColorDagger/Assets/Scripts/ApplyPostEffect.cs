﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ApplyPostEffect : MonoBehaviour {

    public Material postEffect;
    Camera cam;
    void Start()
    {
        cam = GetComponent<Camera>();

        if (cam == null)
        {
            enabled = false;
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (postEffect != null && enabled == true)
        {
            Graphics.Blit(source, destination, postEffect);
        }
    }
}
