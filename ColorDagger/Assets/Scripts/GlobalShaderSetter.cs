﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GlobalShaderSetter : MonoBehaviour {

    Camera cam;
    public static float farPlane = 80;
    private float baseFarPlane;
    public Color farColor;
    public Color nearColor;

    private Color baseFarColor;
    private Color baseNearColor;

    public Texture crosshairImage;
    public bool showCrossHair = false;

    public static GlobalShaderSetter instance;
    private Color blinkback;
    private Color blinkback2;

    private Color blinkCol;
    private Color blinkCol2;

    private void Awake()
    {
        instance = this;
        baseFarColor = farColor;
        baseNearColor = nearColor;
        farColor.r = PlayerPrefs.GetFloat("farR", farColor.r);
        farColor.g = PlayerPrefs.GetFloat("farG", farColor.g);
        farColor.b = PlayerPrefs.GetFloat("farB", farColor.b);

        nearColor.r = PlayerPrefs.GetFloat("nearR", nearColor.r);
        nearColor.g = PlayerPrefs.GetFloat("nearG", nearColor.g);
        nearColor.b = PlayerPrefs.GetFloat("nearB", nearColor.b);

        blinkCol = farColor;
        blinkCol.r = Mathf.Clamp01(farColor.r + 0.03f);
        blinkCol.g = Mathf.Clamp01(farColor.g + 0.03f);
        blinkCol.b = Mathf.Clamp01(farColor.b + 0.03f);

        blinkCol2 = nearColor;
        blinkCol2.r = Mathf.Clamp01(nearColor.r - 0.01f);
        blinkCol2.g = Mathf.Clamp01(nearColor.g - 0.01f);
        blinkCol2.b = Mathf.Clamp01(nearColor.b - 0.01f);

    }

    float blinkTime = 0.2f;
    float lastTime;
    bool haveBlinked = false;
    public void BlinkColor()
    {
        if (haveBlinked == false)
        {
            blinkback = farColor;
            blinkback2 = nearColor;

            farColor = blinkCol;
            nearColor = blinkCol2;
        }

        haveBlinked = true;
        lastTime = Time.time;
    }

    private void Start()
    {
        cam = Camera.main;
        baseFarPlane = farPlane;
    }

    public void GetNewColors()
    {
        farColor = Random.ColorHSV();
        nearColor = Random.ColorHSV();
        SaveNewColor();

        blinkCol = farColor;
        blinkCol.r = Mathf.Clamp01(farColor.r + 0.03f);
        blinkCol.g = Mathf.Clamp01(farColor.g + 0.03f);
        blinkCol.b = Mathf.Clamp01(farColor.b + 0.03f);

        blinkCol2 = nearColor;
        blinkCol2.r = Mathf.Clamp01(nearColor.r - 0.01f);
        blinkCol2.g = Mathf.Clamp01(nearColor.g - 0.01f);
        blinkCol2.b = Mathf.Clamp01(nearColor.b - 0.01f);
    }

    public void ResetColors()
    {
        farColor = baseFarColor;
        nearColor= baseNearColor;
        SaveNewColor();
    }

    public void SaveNewColor()
    {
        PlayerPrefs.SetFloat("farR", farColor.r);
        PlayerPrefs.SetFloat("farG", farColor.g);
        PlayerPrefs.SetFloat("farB", farColor.b);

        PlayerPrefs.SetFloat("nearR", nearColor.r);
        PlayerPrefs.SetFloat("nearG", nearColor.g);
        PlayerPrefs.SetFloat("nearB", nearColor.b);
    }

    void OnGUI()
    {
        if (showCrossHair == false)
            return;

        float xMin = (Screen.width / 2) - (10 / 2);
        float yMin = (Screen.height / 2) - (10/ 2);
        GUI.DrawTexture(new Rect(xMin, yMin, 10, 10), crosshairImage);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            farPlane = baseFarPlane;

        if (Input.GetKeyDown(KeyCode.R))
            GetNewColors();

        if (cam == null)
            Start();

        cam.backgroundColor = farColor;

        Shader.SetGlobalFloat(Shader.PropertyToID("_FarPlane"), farPlane);// cam.farClipPlane);
        Shader.SetGlobalVector(Shader.PropertyToID("_PlayerPos"), Vector3.zero);
        Shader.SetGlobalColor(Shader.PropertyToID("_ColorFar"), farColor);
        Shader.SetGlobalColor(Shader.PropertyToID("_ColorNear"), nearColor);


        if(haveBlinked)
        {
            if (Time.time > blinkTime + lastTime)
            {
                farColor = blinkback;
                nearColor = blinkback2;

                haveBlinked = false;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0,0,1,0.2f);
        Gizmos.DrawSphere(Vector3.zero, farPlane);
    }

}
