﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour
{
    public float nextInMin;
    public float nextInMax;
    private float rnd;
    private float baseTime = 0;
    public bool randomPitch;
    private float basePitch;
    AudioSource source;
    // Use this for initialization
    void Start () {
        rnd = Random.Range(nextInMin, nextInMax);
        baseTime = Time.time;
        source = GetComponent<AudioSource>();
        basePitch = source.pitch;
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if(Time.time > baseTime + rnd)
        {
            if (randomPitch)
                source.pitch = basePitch + Random.Range(-0.3f, 0.3f);

            source.Play();
            rnd = Random.Range(nextInMin, nextInMax);
            baseTime = Time.time;
        }
	}
}
