﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhaleFollower : MonoBehaviour {

    public GameObject toFollow;
    private float followDistance;
    private List<Vector3> storedPositions;

    void Awake()
    {
        
    }

    void Start()
    {
        followDistance = toFollow.transform.localScale.x*4;
        storedPositions = new List<Vector3>(); //create a blank list
    }

    void FixedUpdate()
    {
        if (storedPositions.Count == 0)
        {
            storedPositions.Add(toFollow.transform.position); //store the players currect position
            return;
        }
        else if (storedPositions[storedPositions.Count - 1] != toFollow.transform.position)
        {
            //Debug.Log("Add to list");
            storedPositions.Add(toFollow.transform.position); //store the position every frame
        }

        if (storedPositions.Count > followDistance)
        {
            transform.position = storedPositions[0]; //move
            storedPositions.RemoveAt(0); //delete the position that player just move to
        }
    }
}
