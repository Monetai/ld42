﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    public GameObject toSpawn;
    public GameObject WhalePrefab;

    public float distanceFromCenter = 50;
    public int nbSpawner = 10;

    public float angleToCover = 360f;
    private float delta = 0f;
    public float offset;

    public int tikets = 300;


    public static int usedSeekerTiket = 0;
    public static int usedWhaleTiket = 0;
    private  int maxWhaleTicket = 2;


    private List<GameObject> spawners;

    public float spawnRate = 2f;
    float prevSpawn;
    GameObject player;

    private void Start()
    {
        player = FindObjectOfType<CharacterController>().gameObject;

        spawners = new List<GameObject>();
        delta = angleToCover / nbSpawner;
        for (int i = 0; i < nbSpawner; i++)
        {
            float x = distanceFromCenter * Mathf.Cos((delta * i + offset) * Mathf.Deg2Rad);
            float z = distanceFromCenter * Mathf.Sin((delta * i + offset) * Mathf.Deg2Rad);

            GameObject obj = new GameObject();
            obj.transform.position = transform.position + new Vector3(x, 0, z);
            spawners.Add(obj);
        }

        int spawner = Random.Range(0, spawners.Count);
        for (int i = 0; i < 20; i++)
        {
            if (usedSeekerTiket > tikets)
                continue;
            GameObject obj = Instantiate(toSpawn, spawners[spawner].transform.position + Random.onUnitSphere * Random.Range(5, 10), spawners[0].transform.rotation);
            obj.GetComponent<Boids>().target = player;
            usedSeekerTiket++;
        }
        
        prevSpawn = Time.time;
    }

    private void Update()
    {
        if (Time.time > (prevSpawn + spawnRate))
        {
            if (Random.Range(0, 100) < 30 && usedWhaleTiket <= maxWhaleTicket)
            {
                int spawner = Random.Range(0, spawners.Count);
                GameObject obj = Instantiate(WhalePrefab, spawners[spawner].transform.position + Random.onUnitSphere * Random.Range(5, 10), spawners[0].transform.rotation);
                usedWhaleTiket++;
            }
            else
            {
                int nbToSpawn = 5;
                int spawner = Random.Range(0, spawners.Count);
                for (int i = 0; i < nbToSpawn; i++)
                {
                    if (usedSeekerTiket > tikets)
                        continue;

                    GameObject obj = Instantiate(toSpawn, spawners[spawner].transform.position + Random.onUnitSphere * Random.Range(5, 10), spawners[0].transform.rotation);
                    obj.GetComponent<Boids>().target = player;
                    usedSeekerTiket++;
                }
            }
            prevSpawn = Time.time;
        }
    }
}
