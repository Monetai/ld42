﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraSynchroniser : MonoBehaviour {

    Camera mainCam;
    Camera targetCam;
    // Use this for initialization
    private void Start()
    {
        mainCam = Camera.main;
        targetCam = GetComponent<Camera>();
    }

    private void Update()
    {
        if (mainCam == null || targetCam == null)
        {
            Start();
        }
        targetCam.backgroundColor = mainCam.backgroundColor;
        targetCam.farClipPlane = mainCam.farClipPlane;
        targetCam.fieldOfView = mainCam.fieldOfView;
    }
}
