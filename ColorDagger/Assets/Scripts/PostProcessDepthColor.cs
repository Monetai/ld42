﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PostProcessDepthColor : MonoBehaviour {

    public Material depthColorMat;
    Camera cam;
    void Start()
    {
        cam = GetComponent<Camera>();

        if (cam != null)
        {
            cam.depthTextureMode = DepthTextureMode.Depth;
        }
        else
        {
            enabled = false;
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if(depthColorMat != null)
        {
            Graphics.Blit(source, destination, depthColorMat);
        }
    }
}
